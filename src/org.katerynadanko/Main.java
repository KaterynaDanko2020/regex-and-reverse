package org.katerynadanko;

public class Main {

    public static void main(String[] args) {

        AddressParserImpl addressParser = new AddressParserImpl("08606, Київська обл., Васильківський р-н, С. ДЕРЕВ'ЯНКИ, вул. Боженка, 10/4, кв. 10");

        addressParser.splitAddress();

        System.out.println(addressParser.getPostcode());
        System.out.println(addressParser.getRegion());
        System.out.println(addressParser.getDistrict());
        System.out.println(addressParser.getPlace());
        System.out.println(addressParser.getStreet());
        System.out.println(addressParser.getHouseNumber());
        System.out.println(addressParser.getRoom());
    }
}

