package org.katerynadanko;

// Классы в Java называются с большой буквы
public class ReversedStr {

    // удали это. используй StringBuilder
//    static String reversedString(String str) {
//        byte[] ar = str.getBytes();
//        byte[] res = new byte[ar.length];
//        for (int i = 0; i < ar.length; i++) {
//            res[i] = ar[ar.length - i-1]; }
//        return (new String(res));
//    }
    static String reversedString(String str) {

        String s1=new String(new StringBuilder().append(str).reverse());
        String s = "";
        char ch [] = s1.toCharArray();
        for ( int i=0;i<ch.length; i++) {
            if (i%2==1) {
                ch[i]=Character.toUpperCase(ch[i]);
            }
            else {
                ch[i]=Character.toLowerCase(ch[i]);
            }
            s +=ch[i];
        }
        return s;
    }

    public static void main(String[] args) {

        System.out.println(reversedString(new String("Hello World! My name is App.")));

    }
}
