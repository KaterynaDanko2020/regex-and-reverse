package org.katerynadanko;

// в задании методы возвращают значения строкового типа
public interface AddressParser {
    String getPostcode();

    String getRegion();

    String getDistrict();

    String getPlace();

    String getStreet();

    String getHouseNumber();

    String getRoom();
}
