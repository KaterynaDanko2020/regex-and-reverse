package org.katerynadanko;

public class AddressParserImpl implements AddressParser {

    String str;

    public AddressParserImpl(String str) {
        this.str = str;
    }

    String[] splitAddress() {
        String[] strings = str.split("\\,");
        return strings;
    }


    @Override
    public String getPostcode() {
        String res = "Індекс: ";
        String[] strings = str.split("\\,");
        for (String s : strings) {
            if (s.matches("\\d{5}")) {
                res =res+s;
            }
        }
        return res;
    }

    @Override
    public String getRegion() {
        String res = "Область: ";
        String[] strings = str.split("\\,");
        for (String s : strings) {
            // не матчится потому что у тебя в каждая строка начинается с пробела а ты его не учла
            /*
            (\\s)?[А-ЯІЇЄҐа-яіїєґ\-]+\s(обл(\.|асть))
             */
            if (s.matches("[(\\s)?А-ЯІЇЄҐа-яіїєґ\\-]+\\s(обл(\\.|асть))")) {
                res =res+s;
            }
        }
        return res;
    }

    @Override
    public String getDistrict() {
        String res = "Район: ";
        String[] strings = str.split("\\,");
        for (String s : strings) {

            // (\\s)?([А-ЯІЇЄҐа-яіїєґ\\-]+)(\\s)р(\\-н|айон)
            if (s.matches("((\\s)?[А-ЯІЇЄҐа-яіїєґ\\-]+)(\\s)р(\\-н|айон)")) {
                res =res+s;
            }
        }
        return res;
    }

    @Override
    public String getPlace() {
        String res = "Місто/село: ";
        String[] strings = str.split("\\,");
        for (String s : strings) {

            if (s.matches("((\\s)?[СМ]+)\\.?\\s?[А-ЯІЇЄҐ']+")) {
                res =res+s;
            }
        }
        return res;
    }

    @Override
    public String getStreet() {
        String res = "Вулиця/провулок: ";
        String[] strings = str.split("\\,");
        for (String s : strings) {
            if (s.matches("((\\s)?[провул]{3,6})\\.?\\s?([А-ЯІЇЄҐ'])([а-яіїєґ]+)")) {
                res =res+s;
            }
        }
        return res;
    }

    @Override
    public String getHouseNumber() {
String res = "Будинок: ";
        String[] strings = str.split("\\,");
        for (String s : strings) {
            if (s.matches("(\\s)?[1-9]+\\d?\\/?\\-?\\,?\\d*[А-ЯІЇЄҐ]?")) {
                res =res+s;
            }
        }
        return res;
    }

    @Override
    public String getRoom() {
        String res = "Квартира: ";
        String[] strings = str.split("\\,");
        for (String s : strings) {
            if (s.matches("(\\s)?[кв]+\\.?\\s?[1-9]+\\d*")) {
                res =res+s;
            }
        }
        return res;
    }
}
